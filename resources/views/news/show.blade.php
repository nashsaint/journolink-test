@extends('layout.base')

@section('breadcrumbs')
    <ul class="uk-breadcrumb">
        <li><span class="uk-icon" uk-icon="icon: arrow-left"></span><a href="/">Back to News Feed</a></li>
    </ul>
@endsection

@section('content')
    <article class="uk-article">
        <h1 class="uk-article-title"><a class="uk-link-reset" href="">{{ $news->title }}</a></h1>
        <p class="uk-article-meta">Written by <a href="#">{{ $news->author }}</a> on 12 April 2012. Posted in <a href="#">{{ $news->category }}</a></p>
        <p class="uk-text-lead">{{ $news->description }}</p>
        <p>{!! $news->content !!}</p>

        <div class="uk-grid-small uk-child-width-auto" uk-grid>
            <div>
                <a class="uk-button uk-button-text" href="#">Read more</a>
            </div>
            <div>
                <a class="uk-button uk-button-text" href="#">5 Comments</a>
            </div>
        </div>

    </article>
@endsection