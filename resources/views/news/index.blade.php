@extends('layout.base')

@section('content')
    <div class="uk-child-width-1-3@m uk-grid-small uk-grid-match" uk-grid>
        @foreach($feed as $item)
            <div>
                <div class="uk-card uk-card-default">
                    <div class="uk-card-media-top">
                        <img src="{{ $item->thumbnail }}" alt="">
                    </div>
                    <div class="uk-card-body">
                        <h3 class="uk-card-title">{{ $item->title }}</h3>
                        <p>{{ $item->description }}</p>
                    </div>
                    <div class="uk-card-footer uk-background-primary uk-light">
                        <a href="/news/{{ $item->id }}" class="uk-button uk-button-text">Read more</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection