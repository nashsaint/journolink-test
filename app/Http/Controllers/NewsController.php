<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;
use SimplePie;

/**
 * Class NewsController
 * @package App\Http\Controllers
 */
class NewsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $feed = News::all();

        return view('news.index', [
            'feed' => $feed
        ]);
    }


    /**
     * @param Request $request
     * @param News $id
     */
    public function view(Request $request, News $id)
    {
        $news = News::find($id)->first();

        return view('news.show', [
           'news' => $news,
        ]);
    }
}
