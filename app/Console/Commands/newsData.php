<?php

namespace App\Console\Commands;

use App\News;
use Illuminate\Console\Command;
use SimplePie;

class newsData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch news feed from the source and persist it to the local database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** @var SimplePie $feed */
        $feed = new SimplePie();
        $feed->set_feed_url('https://metro.co.uk/feed/');
        $feed->set_cache_location(base_path() . '/storage/feed');

        $feed->init();

        $feed->handle_content_type();

        /** @var SimplePie $item */
        foreach ($feed->get_items() as $item) {

            // skip if feed is already exist int he db
            if (News::where('feed_id', $item->get_id())->first()) {
                continue;
            }

            if ($enclosure = $item->get_enclosure()) {
                $thumbnail = head($enclosure->get_thumbnails());
            }

            /** @var News $newsItem */
            $newsItem = new News();

            $newsItem->feed_id = $item->get_id();
            $newsItem->title = $item->get_title();
            $newsItem->category = $item->get_category()->term;
            $newsItem->author = $item->get_author()->name;
            $newsItem->permalink = $item->get_permalink();
            $newsItem->description = $item->get_description();
            $newsItem->thumbnail = isset($thumbnail) ? $thumbnail : null;
            $newsItem->content = $item->get_content();

            $newsItem->save();
        }

    }
}
