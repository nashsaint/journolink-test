## Migration
Run migration before fetching news feed

## Command
`php artisan news:fetch` - This command fetches news feed from [Metro](https://metro.co.uk/feed/) and it saves it to local db.

